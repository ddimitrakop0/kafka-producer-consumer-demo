package constants;

import lombok.experimental.UtilityClass;

@UtilityClass
public class TopicNames {
    public static final String TOPIC_MOVIE = "topic-movie";
    public static final String TOPIC_MUSIC = "topic-music";
}
