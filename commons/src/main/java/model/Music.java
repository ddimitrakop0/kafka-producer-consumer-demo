package model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Music model class
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Music {
    private String singer;
    private String title;
}
