package model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Movie model class
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Movie {
    private String title;
    private int date;
}
