package ddimi.kafkaconsumer.service;

import model.Movie;
import model.Music;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import static constants.TopicNames.TOPIC_MOVIE;
import static constants.TopicNames.TOPIC_MUSIC;

/**
 * KAFKA listeners
 */
@Service
public class KafkaConsumer {

    private static final String BEE2BEE = "bee2bee";
    private static final String ANONYMOUS = "Anonymous";

//     Demo load balance within a group

//    @KafkaListener(topics = TOPIC_MOVIE, groupId = BEE2BEE,
//            containerFactory = "stringConcurrentKafkaListenerContainerFactory")
//    public void consume01(String message) {
//
//        System.out.println(BEE2BEE + " -> Consumed string message: " + message);
//    }

//     -------- Consuming both topics for String messages --------

//    @KafkaListener(topics = {TOPIC_MOVIE, TOPIC_MUSIC}, groupId = BEE2BEE,
//            containerFactory = "stringConcurrentKafkaListenerContainerFactory")
//    public void consume0(String message) {
//
//        System.out.println(BEE2BEE + " -> Consumed string message: " + message);
//    }


    // -------- Consuming movie object --------

    @KafkaListener(topics = TOPIC_MOVIE, groupId = BEE2BEE,
            containerFactory = "movieConcurrentKafkaListenerContainerFactory")
    public void consume1(Movie movie) {

        System.out.println(BEE2BEE + " -> Consumed movie (object) message: " + movie);
    }

    // -------- Consuming music object --------

    @KafkaListener(topics = TOPIC_MUSIC, groupId = BEE2BEE,
            containerFactory = "musicConcurrentKafkaListenerContainerFactory")
    public void consume2(Music music) {

        System.out.println(BEE2BEE + " -> Consumed music (object) message: " + music);
    }
}
