package ddimi.kafkaproducer.service;

import lombok.RequiredArgsConstructor;
import model.Movie;
import model.Music;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

import static constants.TopicNames.TOPIC_MOVIE;
import static constants.TopicNames.TOPIC_MUSIC;

/**
 * Controller for testing KAFKA message producing
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/kafka")
public class KafkaController {

    // 1)------------ Publishing String (messages) to topics ------------

    private final KafkaTemplate<String, String> stringKafkaTemplate;

    @GetMapping("/movie/{message}")
    public String postMovieString(@PathVariable("message") String message) {

        stringKafkaTemplate.send(TOPIC_MOVIE, message);
        return "Published Movie (message) successfully!";
    }

    @GetMapping("/music/{message}")
    public String postMusicString(@PathVariable("message") String message) {

        stringKafkaTemplate.send(TOPIC_MUSIC, message);
        return "Published Music (message) successfully!";
    }

    // 2)------------ Publishing String (key,messages) to topics ------------

    @GetMapping("/movie/{message}/{key}")
    public String postMovieStringWithKey(
            @PathVariable("message") String message,
            @PathVariable("key") String key) {

        stringKafkaTemplate.send(TOPIC_MOVIE, key, message);
        return "Published Movie (key, message) successfully";
    }

    // 3)------------ Publishing json (object-class) messages to topics ------------

    private final KafkaTemplate<String, Movie> movieKafkaTemplate;
    private final KafkaTemplate<String, Music> musicKafkaTemplate;

    @PostMapping("/movie")
    public String postMovie(@RequestBody Movie movie) {

        movieKafkaTemplate.send(TOPIC_MOVIE, movie);
        return "Published Movie (object) successfully!";
    }

    @PostMapping("/music")
    public String postMusic(@RequestBody Music music) {

        musicKafkaTemplate.send(TOPIC_MUSIC, music);
        return "Published (Music) (object) successfully!";
    }
}
