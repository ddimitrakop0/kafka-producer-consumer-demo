package ddimi.kafkaproducer.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

import static constants.TopicNames.TOPIC_MOVIE;
import static constants.TopicNames.TOPIC_MUSIC;

/**
 * Class for configuring the KAFKA topics
 */
@Configuration
public class KafkaTopicsConfig {

    private static final int PARTITIONS = 2;

    @Bean
    public NewTopic testingStringTopic() {
        return TopicBuilder
                .name(TOPIC_MOVIE)
                // create more partitions to enhance parallel processing
                .partitions(PARTITIONS)
                .build();
    }

    @Bean
    public NewTopic testingJsonTopic() {
        return TopicBuilder
                .name(TOPIC_MUSIC)
                // create more partitions to enhance parallel processing
                .partitions(PARTITIONS)
                .build();
    }
}
